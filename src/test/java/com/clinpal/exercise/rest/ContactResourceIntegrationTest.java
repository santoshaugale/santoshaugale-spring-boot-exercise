package com.clinpal.exercise.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;
import com.clinpal.exercise.service.contact.impl.ContactRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class ContactResourceIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ContactService contactService;

	@Autowired
	private ContactRepository contactRepository;

	@BeforeEach
	public void init() {
		Contact fred = new Contact(1l, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");

		Contact wilma = new Contact(2l, "Wilma", "Flintstone", "Spine City Road", "Pune", "123456", "Maharashtra",
				"India");

		contactService.save(fred);
		contactService.save(wilma);
	}

	@AfterEach
	public void cleanUpTestData() {
		contactRepository.deleteAll();
	}

	@Test
	public void testFindAllContacts() throws Exception {

		String restUrl = "/api/contacts";

		List<Contact> contacts = contactService.findAll();

		String mvcResponseString = mockMvc.perform(get(restUrl)).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();
		String contactJsonDataString = objectMapper.writeValueAsString(contacts);

		assertThat(mvcResponseString).isEqualToIgnoringCase(contactJsonDataString);

	}

	@Test
	public void testFindAllContactsNoDataFound() throws Exception {

		String restUrl = "/api/contacts";

		this.cleanUpTestData();
		mockMvc.perform(get(restUrl)).andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorMessage").value("No Data Found Error"))
				.andExpect(jsonPath("$.errorCode").value("CNT0001"))
				.andExpect(jsonPath("$.errorDetails", hasItems("User contact details are not present in the system")));
	}

	@Test
	public void testFindContactBySurnameLike() throws Exception {

		String restUrl = "/api/contacts";

		List<Contact> contacts = contactService.findContactBySurnameLike("one");

		MvcResult mvcResponse = mockMvc.perform(get(restUrl + "/one")).andExpect(status().isOk()).andReturn();
		String mvcResponseString = mvcResponse.getResponse().getContentAsString();
		String contactJsonDataString = objectMapper.writeValueAsString(contacts);

		assertThat(mvcResponseString).isEqualToIgnoringCase(contactJsonDataString);

	}

	@Test
	public void testFindContactBySurnameLikeNoDataFound() throws Exception {

		String restUrl = "/api/contacts";

		this.cleanUpTestData();
		mockMvc.perform(get(restUrl + "/one")).andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorMessage").value("No Data Found Error"))
				.andExpect(jsonPath("$.errorCode").value("CNT0001"))
				.andExpect(jsonPath("$.errorDetails", hasItems("User contact details are not present in the system")));

	}

	@Test
	public void testSaveContactContact() throws Exception {

		String restUrl = "/api/contacts";

		Contact fred = new Contact(null, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");

		MvcResult mvcResult = mockMvc.perform(post(restUrl).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(fred))).andExpect(status().isCreated()).andReturn();
		fred = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<Contact>() {
		});

		Contact dbContact = contactRepository.getOne(fred.getId());

		assertTrue(dbContact.getId() == fred.getId());

	}

	@Test
	public void testSaveContactForNullValues() throws Exception {

		String restUrl = "/api/contacts";

		Contact emptyContact = new Contact(null, null, null, null, null, null, null, null);

		mockMvc.perform(post(restUrl).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(emptyContact))).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Surname is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode is required")));
	}

	@Test
	public void testSaveContactForLengthValidation() throws Exception {

		String restUrl = "/api/contacts";

		Contact fred = new Contact(null, "FredFredFredFredFredFredFredFred", "BillingsBillingsBillingsBillings",
				"ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2",
				"Pune", "4113099090", "Maharashtra", "India");

		mockMvc.perform(post(restUrl).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(fred))).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode should contains 6-8 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Street should not have more than 100 characters")));

	}

	@Test
	public void testUpdateContact() throws Exception {

		String restUrl = "/api/contacts";

		List<Contact> contacts = contactService.findAll();
		Contact dbContact = contacts.get(0);
		dbContact.setForename("Fred_updated");

		MvcResult mvcResult = mockMvc.perform(put(restUrl + "/" + dbContact.getId())
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(dbContact)))
				.andExpect(status().isOk()).andReturn();
		Contact updatedContact = objectMapper.readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<Contact>() {
				});

		assertTrue(dbContact.getId() == updatedContact.getId()
				&& dbContact.getForename().equals(updatedContact.getForename()));

	}

	@Test
	public void testUpdateContactForNullValues() throws Exception {

		String restUrl = "/api/contacts";

		List<Contact> contacts = contactService.findAll();
		Contact dbContact = contacts.get(0);
		dbContact.setForename(null);
		dbContact.setSurname(null);
		dbContact.setPostalCode(null);

		mockMvc.perform(put(restUrl + "/" + dbContact.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(dbContact))).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Surname is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode is required")));
	}

	@Test
	public void testUpdateContactForLengthValidation() throws Exception {

		String restUrl = "/api/contacts";

		List<Contact> contacts = contactService.findAll();
		Contact dbContact = contacts.get(0);
		dbContact.setForename("FredFredFredFredFredFredFredFred");
		dbContact.setSurname("BillingsBillingsBillingsBillings");
		dbContact.setStreet(
				"ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2");
		dbContact.setPostalCode("4113099090");

		mockMvc.perform(put(restUrl + "/" + dbContact.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(dbContact))).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode should contains 6-8 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Street should not have more than 100 characters")));

	}

	@Test
	public void testUpdateContactForNoDataFound() throws Exception {

		String restUrl = "/api/contacts";

		this.cleanUpTestData();

		Contact fred = new Contact(1L, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");

		mockMvc.perform(put(restUrl + "/" + fred.getId()).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(fred))).andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorMessage").value("No Data Found Error"))
				.andExpect(jsonPath("$.errorCode").value("CNT0002")).andExpect(jsonPath("$.errorDetails",
						hasItems("User contact details are not found with id:" + fred.getId())));

	}

}
