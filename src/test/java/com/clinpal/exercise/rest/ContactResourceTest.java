package com.clinpal.exercise.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.clinpal.exercise.exception.ResourceNotFoundException;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ContactResource.class)
public class ContactResourceTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ContactService contactService;

	@Autowired
	private ObjectMapper objectMapper;

	private static List<Contact> contacts = new ArrayList<>();

	@BeforeAll
	public static void init() {

		Contact fred = new Contact(1l, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");
		contacts.add(fred);

		Contact wilma = new Contact(2l, "Wilma", "Flintstone", "Spine City Road", "Pune", "123456", "Maharashtra",
				"India");
		contacts.add(wilma);
	}

	@Test
	public void testFindAllContacts() throws Exception {

		String restUrl = "/api/contacts";

		when(contactService.findAll()).thenReturn(contacts);

		MvcResult mvcResponse = mockMvc.perform(get(restUrl)).andExpect(status().isOk()).andReturn();
		String mvcResponseString = mvcResponse.getResponse().getContentAsString();
		String contactJsonDataString = objectMapper.writeValueAsString(contacts);

		assertThat(mvcResponseString).isEqualToIgnoringCase(contactJsonDataString);

	}

	@Test
	public void testFindAllContactsNoDataFound() throws Exception {

		String restUrl = "/api/contacts";

		when(contactService.findAll()).thenThrow(
				new ResourceNotFoundException("CNT0001", "User contact details are not present in the system"));

		mockMvc.perform(get(restUrl)).andExpect(status().isNotFound());
	}

	@Test
	public void testFindContactBySurnameLike() throws Exception {

		String restUrl = "/api/contacts";

		when(contactService.findContactBySurnameLike("one")).thenReturn(contacts);

		MvcResult mvcResponse = mockMvc.perform(get(restUrl + "/one")).andExpect(status().isOk()).andReturn();
		String mvcResponseString = mvcResponse.getResponse().getContentAsString();
		String contactJsonDataString = objectMapper.writeValueAsString(contacts);

		assertThat(mvcResponseString).isEqualToIgnoringCase(contactJsonDataString);

	}

	@Test
	public void testFindContactBySurnameLikeNoDataFound() throws Exception {

		String restUrl = "/api/contacts";

		when(contactService.findContactBySurnameLike("one")).thenThrow(
				new ResourceNotFoundException("CNT0001", "User contact details are not present in the system"));

		mockMvc.perform(get(restUrl + "/one")).andExpect(status().isNotFound());

	}

	@Test
	public void testSaveContact() throws Exception {

		String restUrl = "/api/contacts";

		Contact sam = new Contact(null, "Sam", "Billing", "DP Road", "Pune", "123456", "Maharashtra", "India");
		when(contactService.save(sam)).thenReturn(sam);

		mockMvc.perform(
				post(restUrl).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(sam)))
				.andExpect(status().isCreated());

	}

	@Test
	public void testSaveContactWithNullValues() throws Exception {

		String restUrl = "/api/contacts";

		Contact emptyContact = new Contact(null, null, null, null, null, null, null, null);
		when(contactService.save(emptyContact)).thenReturn(emptyContact);

		mockMvc.perform(post(restUrl).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(emptyContact))).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Surname is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode is required")));

	}

	@Test
	public void testSaveContactForLengthValidation() throws Exception {

		String restUrl = "/api/contacts";

		Contact fred = new Contact(null, "FredFredFredFredFredFredFredFred", "BillingsBillingsBillingsBillings",
				"ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2",
				"Pune", "4113099090", "Maharashtra", "India");
		when(contactService.save(fred)).thenReturn(fred);

		mockMvc.perform(
				post(restUrl).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(fred)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Surname should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode should contains 6-8 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Street should not have more than 100 characters")));

	}

	@Test
	public void testUpdateContact() throws Exception {

		String restUrl = "/api/contacts";

		Contact sam = new Contact(1l, "Sam", "Billing", "DP Road", "Pune", "123456", "Maharashtra", "India");
		when(contactService.update(1l, sam)).thenReturn(sam);

		mockMvc.perform(put(restUrl + "/1").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(sam))).andExpect(status().isOk());
	}

	@Test
	public void testUpdateContactForNullValues() throws Exception {

		String restUrl = "/api/contacts";

		Contact sam = new Contact(1l, null, null, null, null, null, null, null);
		when(contactService.update(1l, sam)).thenReturn(sam);

		mockMvc.perform(put(restUrl + "/1").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(sam))).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Surname is required")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode is required")));

	}

	@Test
	public void testUpdateContactForLengthValidation() throws Exception {

		String restUrl = "/api/contacts";

		Contact fred = new Contact(1l, "FredFredFredFredFredFredFredFred", "BillingsBillingsBillingsBillings",
				"ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2 ABC Tower Road Lane2",
				"Pune", "4113099090", "Maharashtra", "India");
		when(contactService.update(fred.getId(), fred)).thenReturn(fred);

		mockMvc.perform(put(restUrl + "/" + fred.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(fred))).andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errorMessage").value("Input Validation Failed"))
				.andExpect(jsonPath("$.errorCode").value("400"))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Forename should not have more than 30 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("PostalCode should contains 6-8 characters")))
				.andExpect(jsonPath("$.errorDetails", hasItems("Street should not have more than 100 characters")));

	}

	@Test
	public void testUpdateContactForNoDataFound() throws Exception {

		String restUrl = "/api/contacts";

		Contact sam = new Contact(1l, "Sam", "Billing", "DP Road", "Pune", "123456", "Maharashtra", "India");
		when(contactService.update(sam.getId(), sam)).thenThrow(
				new ResourceNotFoundException("CNT0002", "User contact details are not found with id:" + sam.getId()));

		mockMvc.perform(put(restUrl + "/" + sam.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(sam))).andExpect(status().isNotFound());

	}

	@Test
	public void testHttpMessageNotReadableException() throws Exception {

		String restUrl = "/api/contacts";

		mockMvc.perform(
				post(restUrl).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(null)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testTypeMismatchException() throws Exception {

		String restUrl = "/api/contacts";

		Contact sam = new Contact(1l, "Sam", "Billing", "DP Road", "Pune", "123456", "Maharashtra", "India");

		mockMvc.perform(put(restUrl + "/1s").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(sam))).andExpect(status().isBadRequest());
	}

	@Test
	public void testHttpRequestMethodNotSupported() throws Exception {

		String restUrl = "/api/contacts";

		Contact sam = new Contact(1l, "Sam", "Billing", "DP Road", "Pune", "123456", "Maharashtra", "India");

		mockMvc.perform(post(restUrl + "/" + sam.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(sam))).andExpect(status().isMethodNotAllowed());

	}

}
