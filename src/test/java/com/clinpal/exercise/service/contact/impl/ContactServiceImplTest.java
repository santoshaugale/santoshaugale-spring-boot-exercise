package com.clinpal.exercise.service.contact.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.clinpal.exercise.exception.ResourceNotFoundException;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

@SpringBootTest
class ContactServiceImplTest {

	@Autowired
	private ContactService contactService;

	@MockBean
	private ContactRepository contactRepository;

	@Test
	public void testFindAllContacts() {

		List<Contact> contacts = new ArrayList<>();
		Contact fred = new Contact(1l, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");
		contacts.add(fred);

		Contact wilma = new Contact(2l, "Wilma", "Flintstone", "Spine City Road", "Pune", "123456", "Maharashtra",
				"India");
		contacts.add(wilma);

		when(contactRepository.findAll()).thenReturn(contacts);
		assertThat(contactService.findAll(), hasItems(fred, wilma));

	}

	@Test
	public void testFindAllContactNoData() {
		List<Contact> contacts = new ArrayList<>();
		when(contactRepository.findAll()).thenReturn(contacts);

		assertThrows(ResourceNotFoundException.class, () -> {
			contactService.findAll();
		}).getErrorCode().equals("CNT0001");

	}

	@Test
	public void testFindContactBySurnameLike() {

		List<Contact> contacts = new ArrayList<>();
		Contact fred = new Contact(1l, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");
		contacts.add(fred);

		Contact wilma = new Contact(2l, "Wilma", "Flintstone", "Spine City Road", "Pune", "123456", "Maharashtra",
				"India");
		contacts.add(wilma);

		when(contactRepository.findContactBySurnameContainingIgnoreCase("one")).thenReturn(contacts);
		assertThat(contactService.findContactBySurnameLike("one"), hasItems(fred, wilma));

	}

	@Test
	public void testFindContactBySurnameLikeNoData() {
		List<Contact> contacts = new ArrayList<>();
		when(contactRepository.findContactBySurnameContainingIgnoreCase("one")).thenReturn(contacts);

		assertThrows(ResourceNotFoundException.class, () -> {
			contactService.findContactBySurnameLike("one");
		}).getErrorCode().equals("CNT0001");
	}

	@Test
	public void testCreateContact() {
		Contact fred = new Contact(null, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");
		when(contactRepository.save(fred)).thenReturn(fred);
		assertThat(contactService.save(fred).toString().equals(fred.toString()));

	}

	@Test
	public void testUpdateContact() {
		Contact fred = new Contact(1l, "Fred", "Flintstone", "DP Road", "Pune", "123456", "Maharashtra", "India");

		when(contactRepository.findById(1l)).thenReturn(Optional.of(fred));
		fred.setForename("Fred_updated");
		when(contactRepository.save(fred)).thenReturn(fred);
		assertThat(contactService.update(1l, fred).getForename().equals(fred.getForename()));

	}

	@Test
	public void testUpdateContactNoData() {
		assertThrows(ResourceNotFoundException.class, () -> {
			contactService.update(1l, null);
		}).getErrorCode().equals("CNT0002");

	}

}
