package com.clinpal.exercise.service.contact.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.clinpal.exercise.service.contact.domain.Contact;

/**
 * Tests the ContactRepository
 */
@SpringBootTest
class ContactRepositoryTest {

	@MockBean
	private ContactRepository contactRepository;

	@Test
	public void findContactBySurnameLike() {

		final Contact fred = new Contact();
		fred.setForename("Fred");
		fred.setSurname("Flintstone");

		final Contact wilma = new Contact();
		wilma.setForename("Wilma");
		wilma.setSurname("Flintstone");

		final Contact barney = new Contact();
		barney.setForename("Barney");
		barney.setSurname("Rubble");

		List<Contact> contacts = Arrays.asList(fred, wilma, barney);
		when(contactRepository.findAll()).thenReturn(contacts);
		assertThat(contactRepository.findAll(), hasItems(fred, wilma, barney));

		contacts = Arrays.asList(fred, wilma);
		when(contactRepository.findContactBySurnameContainingIgnoreCase("Flint")).thenReturn(contacts);
		assertThat(contactRepository.findContactBySurnameContainingIgnoreCase("Flint"), hasItems(fred, wilma));
	}

}
