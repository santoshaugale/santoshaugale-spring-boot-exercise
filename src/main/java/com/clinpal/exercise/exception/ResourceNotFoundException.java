/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.exception;

/**
 * The Class ResourceNotFoundException.
 *
 * @author imills
 */
public class ResourceNotFoundException extends BaseException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new resource not found exception.
	 *
	 * @param errorCode the error code
	 * @param message   the message
	 * @param cause     the cause
	 */
	public ResourceNotFoundException(String errorCode, String message, Throwable cause) {
		super(errorCode, message, cause);

	}

	/**
	 * Instantiates a new resource not found exception.
	 *
	 * @param errorCode the error code
	 * @param message   the message
	 */
	public ResourceNotFoundException(String errorCode, String message) {
		super(errorCode, message);

	}

	/**
	 * Instantiates a new resource not found exception.
	 */
	public ResourceNotFoundException() {
		super();
	}

}
