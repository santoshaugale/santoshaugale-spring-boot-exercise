/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * The Class CustomExceptionHandler.
 *
 * @author imills
 */
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * Handle no data found exception.
	 *
	 * @param exception the exception
	 * @return the response entity
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handleNoDataFoundException(ResourceNotFoundException exception) {
		CommonResponseErrorData commonResponseErrorData = this.populateCommonErrorData("No Data Found Error", exception,
				HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(commonResponseErrorData, HttpStatus.NOT_FOUND);
	}

	/**
	 * Handle http request method not supported.
	 *
	 * @param ex      the ex
	 * @param headers the headers
	 * @param status  the status
	 * @param request the request
	 * @return the response entity
	 */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		CommonResponseErrorData commonResponseErrorData = this.populateCommonErrorData(
				"Method Not Supported : " + ex.getMethod(), ex, HttpStatus.METHOD_NOT_ALLOWED.value());
		return new ResponseEntity<>(commonResponseErrorData, HttpStatus.METHOD_NOT_ALLOWED);
	}

	/**
	 * Handle type mismatch.
	 *
	 * @param ex      the ex
	 * @param headers the headers
	 * @param status  the status
	 * @param request the request
	 * @return the response entity
	 */
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		CommonResponseErrorData commonResponseErrorData = this.populateCommonErrorData("Type Missmatch Exception", ex,
				HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(commonResponseErrorData, HttpStatus.BAD_REQUEST);

	}

	/**
	 * Handle http message not readable.
	 *
	 * @param ex      the ex
	 * @param headers the headers
	 * @param status  the status
	 * @param request the request
	 * @return the response entity
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		CommonResponseErrorData commonResponseErrorData = this
				.populateCommonErrorData("Required request body is missing", ex, HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(commonResponseErrorData, HttpStatus.BAD_REQUEST);

	}

	/**
	 * Handle method argument not valid.
	 *
	 * @param ex      the ex
	 * @param headers the headers
	 * @param status  the status
	 * @param request the request
	 * @return the response entity
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		StringBuilder errorCode = new StringBuilder().append(status.value());
		
		List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());

		CommonResponseErrorData commonResponseErrorData = new CommonResponseErrorData("Input Validation Failed",
				errorCode.toString());
		commonResponseErrorData.getErrorDetails().addAll(errors);

		return new ResponseEntity<>(commonResponseErrorData, headers, status);
	}

	/**
	 * Populate common error data.
	 *
	 * @param errorMessage the error message
	 * @param exception    the exception
	 * @param status       the status
	 * @return the common response error data
	 */
	private CommonResponseErrorData populateCommonErrorData(String errorMessage, Exception exception, int status) {

		StringBuilder errorCode = new StringBuilder();
		if (exception instanceof BaseException) {
			if (((BaseException) exception).getErrorCode() == null) {
				errorCode.append(status);
			} else {
				errorCode.append(((BaseException) exception).getErrorCode());
			}
		} else {
			errorCode.append(status);
		}
		CommonResponseErrorData commonResponseErrorData = new CommonResponseErrorData(errorMessage,
				errorCode.toString());
		commonResponseErrorData.getErrorDetails().add(exception.getMessage());

		return commonResponseErrorData;
	}

}
