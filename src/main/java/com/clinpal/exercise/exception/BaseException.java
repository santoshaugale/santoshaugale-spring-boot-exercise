/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.exception;

/**
 * The Class BaseException.
 *
 * @author imills
 */
public class BaseException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The error code. */
	private String errorCode;

	/**
	 * Instantiates a new base exception.
	 */
	public BaseException() {
		super();
	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param errorCode the error code
	 * @param message   the message
	 * @param cause     the cause
	 */
	public BaseException(String errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param errorCode the error code
	 * @param message   the message
	 */
	public BaseException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param message the message
	 */
	public BaseException(String message) {
		super(message);
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
