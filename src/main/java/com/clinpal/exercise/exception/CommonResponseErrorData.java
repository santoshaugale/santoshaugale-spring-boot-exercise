/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class CommonResponseErrorData.
 *
 * @author imills
 */
public class CommonResponseErrorData {

	/** The error message. */
	private String errorMessage;

	/** The error code. */
	private String errorCode;

	/** The error details. */
	private List<String> errorDetails;

	/**
	 * Instantiates a new comman error data.
	 *
	 * @param errorMessage the error message
	 * @param errorCode    the error code
	 */
	public CommonResponseErrorData(String errorMessage, String errorCode) {
		super();
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}

	/**
	 * Instantiates a new comman error data.
	 *
	 * @param errorMessage the error message
	 * @param errorDetails the error details
	 * @param errorCode    the error code
	 */
	public CommonResponseErrorData(String errorMessage, List<String> errorDetails, String errorCode) {
		super();
		this.errorMessage = errorMessage;
		this.getErrorDetails().addAll(errorDetails);
		this.errorCode = errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the new error message
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the error details.
	 *
	 * @return the error details
	 */
	public List<String> getErrorDetails() {
		if (errorDetails == null)
			errorDetails = new ArrayList<String>();
		return errorDetails;
	}

	/**
	 * Sets the error details.
	 *
	 * @param errorDetails the new error details
	 */
	public void setErrorDetails(List<String> errorDetails) {
		this.errorDetails = errorDetails;
	}

}
