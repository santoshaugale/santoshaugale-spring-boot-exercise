/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

/**
 * The REST API for the Contact Service.
 *
 * @author imills
 */
@RestController
@RequestMapping("/api")
public class ContactResource {

	/** The contact service. */
	private final ContactService contactService;

	/**
	 * Instantiates a new contact resource.
	 *
	 * @param contactService the contact service
	 */
	@Autowired
	public ContactResource(final ContactService contactService) {
		this.contactService = contactService;
	}

	/**
	 * REST API to obtain all contacts.
	 *
	 * @return all of the contacts as JSON
	 */
	@GetMapping(value = "/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Contact>> getContacts() {
		return ResponseEntity.ok(contactService.findAll());
	}

	/**
	 * Rest API to get the list of contact with a like match to the supplied.
	 *
	 * @param surname the wildcard surname of contacts to find
	 * @return all of the matching contacts as JSON
	 */
	@GetMapping(value = "/contacts/{surname}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Contact>> getContactBySurname(@PathVariable @Min(1) String surname) {
		return ResponseEntity.ok(contactService.findContactBySurnameLike(surname));
	}

	/**
	 * Rest API to create contact.
	 *
	 * @param contact the contact JSON to save
	 * @return the created contact JSON
	 */
	@PostMapping(value = "/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Contact> saveContact(@Valid @RequestBody Contact contact) {
		return new ResponseEntity<>(contactService.save(contact), HttpStatus.CREATED);
	}

	/**
	 * Rest API to update contact details.
	 *
	 * @param contact the contact JSON to update
	 * @return the updated contact JSON
	 */
	@PutMapping(value = "/contacts/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Contact> updateContact(@PathVariable long id, @Valid @RequestBody Contact contact) {
		return ResponseEntity.ok(contactService.update(id, contact));
	}

}
