/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.service.contact.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clinpal.exercise.exception.ResourceNotFoundException;
import com.clinpal.exercise.service.contact.ContactService;
import com.clinpal.exercise.service.contact.domain.Contact;

/**
 * The concrete implementation of the contact service
 */
@Service
class ContactServiceImpl implements ContactService {

	/** The repository DOA layer */
	private final ContactRepository repository;

	@Autowired
	private ContactServiceImpl(final ContactRepository repository) {
		this.repository = repository;
	}

	@Override
	public Contact save(final Contact contact) {
		return repository.save(contact);
	}

	@Override
	public Contact update(final long contactId, final Contact contact) throws ResourceNotFoundException {

		Contact existingContact = repository.findById(contactId)
				.orElseThrow(() -> new ResourceNotFoundException("CNT0002",
						"User contact details are not found with id:" + contactId));
		BeanUtils.copyProperties(contact, existingContact, "id");
		return repository.save(existingContact);

	}

	@Override
	public List<Contact> findContactBySurnameLike(final String surname) throws ResourceNotFoundException {

		final List<Contact> contacts = repository.findContactBySurnameContainingIgnoreCase(surname);
		if (contacts.isEmpty())
			throw new ResourceNotFoundException("CNT0001", "User contact details are not present in the system");
		return contacts;

	}

	@Override
	public List<Contact> findAll() throws ResourceNotFoundException {
		final List<Contact> contacts = repository.findAll();
		if (contacts.isEmpty())
			throw new ResourceNotFoundException("CNT0001", "User contact details are not present in the system");
		return contacts;
	}

}
