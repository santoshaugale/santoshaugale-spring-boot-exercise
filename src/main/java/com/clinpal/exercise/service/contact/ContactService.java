/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.service.contact;

import java.util.List;

import com.clinpal.exercise.exception.ResourceNotFoundException;
import com.clinpal.exercise.service.contact.domain.Contact;

/**
 * The contact service.
 *
 * @author imills
 */
public interface ContactService {

	/**
	 * Saves a contact to the database. If the contact does not exist is inserted
	 * and it's id is populated.
	 *
	 * @param contact the contact to save
	 * @return the saved contact
	 */
	Contact save(final Contact contact);

	/**
	 * Update existing contact. If contact is not present in database then throws
	 * the resource not found exception.
	 *
	 * @param id      the contact id
	 * @param contact the contact to save
	 * @return the updated contact
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	Contact update(final long id, final Contact contact) throws ResourceNotFoundException;

	/**
	 * Obtains a list of contact with a like match to the supplied.
	 *
	 * @param surname the wildcard surname of contacts to find
	 * @return Obtains a list of matching contacts
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	List<Contact> findContactBySurnameLike(final String surname) throws ResourceNotFoundException;

	/**
	 * Obtains a list of contact all contacts.
	 *
	 * @return Obtains a list of all contacts
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	List<Contact> findAll() throws ResourceNotFoundException;

}
