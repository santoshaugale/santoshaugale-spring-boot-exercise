/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.service.contact.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Contact Entity.
 */
@Entity
public class Contact implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Entity ID. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	/** The version used for optimistic locking. */
	@Version
	@Column(name = "VERSION")
	private Integer version;

	/** The forename. */
	@NotBlank(message = "Forename is required")
	@Size(max = 30, message = "Forename should not have more than {max} characters")
	private String forename;

	/** The surname. */
	@NotBlank(message = "Surname is required")
	@Size(max = 30, message = "Surname should not have more than {max} characters")
	private String surname;

	/** The street. */
	@Size(max = 100, message = "Street should not have more than {max} characters")
	private String street;

	/** The city. */
	private String city;

	/** The postal code. */
	@NotBlank(message = "PostalCode is required")
	@Size(min = 6, max = 8, message = "PostalCode should contains {min}-{max} characters")
	private String postalCode;

	/**
	 * Instantiates a new contact.
	 *
	 * @param id         the id
	 * @param forename   the forename
	 * @param surname    the surname
	 * @param street     the street
	 * @param city       the city
	 * @param postalCode the postal code
	 * @param county     the county
	 * @param country    the country
	 */
	public Contact(Long id, String forename, String surname, String street, String city, String postalCode,
			String county, String country) {
		super();
		this.id = id;
		this.forename = forename;
		this.surname = surname;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
		this.county = county;
		this.country = country;
	}

	/** The county. */
	private String county;

	/** The country. */
	private String country;

	/**
	 * Instantiates a new contact.
	 */
	public Contact() {

	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the forename.
	 *
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * Sets the forename.
	 *
	 * @param forename the new forename
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * Gets the surname.
	 *
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Sets the surname.
	 *
	 * @param surname the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * Gets the street.
	 *
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street.
	 *
	 * @param street the new street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postalCode the new postal code
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Gets the county.
	 *
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * Sets the county.
	 *
	 * @param county the new county
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Contact{" + "id=" + id + ", forename='" + forename + '\'' + ", surname='" + surname + '\''
				+ ", street='" + street + '\'' + ", city='" + city + '\'' + ", postalCode='" + postalCode + '\''
				+ ", county='" + county + '\'' + ", country='" + country + '\'' + '}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Contact)) {
			return false;
		}
		final Contact contact = (Contact) o;
		return Objects.equals(id, contact.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
