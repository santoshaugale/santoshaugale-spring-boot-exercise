/*
 * Copyright (C) 2021 e-clinical health contact POC - All Rights Reserved
 */
package com.clinpal.exercise.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

/**
 * The Class SwaggerConfiguration.
 *
 * @author imills
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	/**
	 * Product api.
	 *
	 * @return the docket
	 */
	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.clinpal.exercise")).paths(regex("/api/contacts.*"))
				.build().apiInfo(metaData());
	}

	/**
	 * Meta data.
	 *
	 * @return the api info
	 */
	private ApiInfo metaData() {
		ApiInfo apiInfo = new ApiInfo("Spring Boot REST API", "Spring Boot REST API for User Contacts", "1.0",
				"Terms of service", new Contact("eclinicalhealth", null, null), null, null);
		return apiInfo;
	}
}
